package com.pi.shoppingcart;

import com.pi.shoppingcart.abstracttest.AbstractJpaTest;
import com.pi.shoppingcart.domain.ShoppingCard;
import com.pi.shoppingcart.repositories.ShoppingCardRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class ShoppingCardRepositoryIT extends AbstractJpaTest {

    public static final String SHOPPING_CARD_NAME = "Some Name";
    @Autowired
    public ShoppingCardRepository shoppingCardRepository;

    @Test
    public void shoppingCardTest() {
        ShoppingCard shoppingCard = new ShoppingCard()
                .setName(SHOPPING_CARD_NAME)
                .setCreationDate(LocalDateTime.now());
        shoppingCardRepository.save(shoppingCard);

        List<ShoppingCard> shoppingCardList = shoppingCardRepository.findAll();
        assertThat(shoppingCardList).isNotNull();
        assertThat(shoppingCardList.size()).isEqualTo(1);
        assertThat(shoppingCardList.get(0).getName()).isEqualTo(SHOPPING_CARD_NAME);
    }
}
