package com.pi.shoppingcart;


import com.pi.shoppingcart.abstracttest.AbstractIntegrationTest;
import com.pi.shoppingcart.abstracttest.AbstractJpaTest;
import com.pi.shoppingcart.repositories.ShoppingCardRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;

import static org.assertj.core.api.Assertions.assertThat;

public class BootstrapTest extends AbstractIntegrationTest {

    @Autowired
    public ShoppingCardRepository shoppingCardRepository;

    @Test
    public void testRun(){
        long count = shoppingCardRepository.count();
        assertThat(count).isEqualTo(20L);
    }
}
