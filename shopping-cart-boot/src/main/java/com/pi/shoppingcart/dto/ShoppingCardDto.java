package com.pi.shoppingcart.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;


@Data
@NoArgsConstructor
@Accessors(chain = true)
public class ShoppingCardDto {
    private String name;
    private LocalDateTime creationDate;
    private LocalDateTime modificationDate;
}
