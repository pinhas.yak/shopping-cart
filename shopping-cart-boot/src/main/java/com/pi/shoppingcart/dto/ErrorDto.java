package com.pi.shoppingcart.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Setter
@Getter
@Accessors(chain = true)
@ToString
public class ErrorDto {
    private int errorCode;
    private String message;
    private String detail;
}
