package com.pi.shoppingcart.service;

import com.pi.shoppingcart.domain.ShoppingCard;
import com.pi.shoppingcart.dto.ShoppingCardDto;
import com.pi.shoppingcart.repositories.ShoppingCardRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ShoppingCardService {
    private final ShoppingCardRepository shoppingCardRepository;

    public List<ShoppingCardDto> getShoppingCards(){
        return shoppingCardRepository
                .findAll()
                .stream()
                .map(this::asDto)
                .collect(Collectors.toList());
    }

    private ShoppingCardDto asDto(ShoppingCard shoppingCard){
        return new ShoppingCardDto()
                .setName(shoppingCard.getName())
                .setCreationDate(shoppingCard.getCreationDate())
                .setModificationDate(shoppingCard.getModificationDate());
    }
}
