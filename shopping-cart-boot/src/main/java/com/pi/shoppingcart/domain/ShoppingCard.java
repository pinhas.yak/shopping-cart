package com.pi.shoppingcart.domain;

import jakarta.persistence.Column;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;

import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@Accessors(chain = true)
public class ShoppingCard {
    @Id
    @GeneratedValue
    private Long id;
    @Column(unique=true)
    private String name;
    @Column(name="creation_date")
    private LocalDateTime creationDate;

    @Column(name="modification_date")
    private LocalDateTime modificationDate;
}
