package com.pi.shoppingcart.bootsrap;


import com.pi.shoppingcart.domain.ShoppingCard;
import com.pi.shoppingcart.repositories.ShoppingCardRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Component
@AllArgsConstructor
@Slf4j
public class DataInitializer implements CommandLineRunner {

    private final ShoppingCardRepository shoppingCardRepository;

    @Override
    public void run(String... args) throws Exception {
        shoppingCardRepository.deleteAll();

        List<ShoppingCard> shoppingCardLists = Stream.generate(new Random()::nextInt)
                .limit(20)
                .map(this::ctreateShoppingCard)
                .collect(Collectors.toList());

        shoppingCardRepository.saveAll(shoppingCardLists);
    }
    private ShoppingCard ctreateShoppingCard(int number){
        return new ShoppingCard()
                .setName("ShoppingCardTitle-"+ number)
                .setCreationDate(LocalDateTime.now());
    }
}
