package com.pi.shoppingcart.controller;


import com.pi.shoppingcart.dto.ShoppingCardDto;
import com.pi.shoppingcart.service.ShoppingCardService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/shoppingcard")
@AllArgsConstructor

public class ShoppingCardController {
    private final ShoppingCardService shoppingCardService;

    @ResponseStatus(HttpStatus.OK)
    @GetMapping()
    public List<ShoppingCardDto> getCountries(){
        return shoppingCardService.getShoppingCards();
    }

}
