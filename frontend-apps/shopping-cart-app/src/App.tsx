import React from 'react';
import { AppRoutes } from './route/AppRoutes';

function App() {
  return (
    <div className="App">
      <AppRoutes/>
    </div>
  );
}

export default App;
