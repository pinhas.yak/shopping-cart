import {FC, useEffect, useState} from "react";
import { DataGrid, GridColDef} from '@mui/x-data-grid';

type ShoppingCardType ={
    name: string,
    creationDate: string,
    modificationDate?:string,
}

const columns: GridColDef[] = [
    { field: 'name', headerName: 'Name', width: 150 },
    { field: 'creationDate', headerName: 'Creation Date', width: 150 },
    { field: 'modificationDate', headerName: 'Modification Date', width: 150 },
];


export const ShoppingCard: FC<{}> = () => {
    const [shoppingCards, setShoppingCards] = useState<ShoppingCardType[]>([]);

    const fetchData = async () => {
        const url = "http://localhost:8080/api/shoppingcard";
        const headers = {}
        return fetch(url, {
            method : "GET",
            mode: 'cors',
            headers: headers
        })
            .then((response) => response.json())
            .then((data) => setShoppingCards(data));
    }

    useEffect(() => {
        fetchData();
    },[])
    console.log(shoppingCards)
    return ( <div style={{ height: 400, width: '100%' }}>
        <DataGrid
            rows={shoppingCards}
            columns={columns}
            getRowId={(row: any) =>  row.name}
            checkboxSelection
        />
    </div>)
}