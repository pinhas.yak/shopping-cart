import React, {FC, ReactNode} from 'react';

type RootWrapperPropsType ={
    children?: ReactNode;
}
const RootWrapper: FC<RootWrapperPropsType> = ({children}) =>{
    return (<>
        <div>{children}</div>
    </>);
}
export default RootWrapper;