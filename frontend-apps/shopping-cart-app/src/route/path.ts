export enum ROUTE {
  DEFAULT = '*',
  HOME = '/',
  GENERAL_SETTINGS = '/general-settings',
}
