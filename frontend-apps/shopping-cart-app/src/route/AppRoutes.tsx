import React, {FC} from 'react';
import {BrowserRouter} from "react-router-dom";
import RoutesContainer from "./RoutesContainer";

export const AppRoutes: FC<{}> = () => {
    return (
        <BrowserRouter><RoutesContainer/></BrowserRouter>
    );
};
