import {FC} from "react";
import {Route, Routes} from "react-router-dom";
import ResponsiveAppBar from "../components/navbar/ResponsiveAppBar";
import {ShoppingCard} from "../components/shoppingcard/ShoppingCard";

const RoutesContainer: FC<{}> = () =>{
    return(
        <>
            <ResponsiveAppBar/>
            <Routes>
                <Route path='/' element={<h1>Home</h1>} />
                <Route path='/products' element={<ShoppingCard/>} />
                <Route path='/pricing' element={<h1>Pricing</h1>} />
                <Route path='/blog' element={<h1>Blog</h1>} />
            </Routes>
    </>
    );
}
export default RoutesContainer;